package deloitte.academy.lesson01;

import deloitte.academy.lesson01.arithmetics.Arithmetics;
import deloitte.academy.lesson01.comparisons.Comparisons;
import deloitte.academy.lesson01.logicalOperators.LogicalOperators;

/**
 * 
 * @author dmascott
 *
 */

public class Ejecutar {
	public static void main(String[] args) {
		/*
		 * Instancia a la clase Arithmetics
		 */
		Arithmetics arithmetics = new Arithmetics();

		/*
		 * Instancia a la clase Comparisons
		 */
		Comparisons comparisons = new Comparisons();

		/*
		 * Instancia a la clase LogicalOperators
		 */
		LogicalOperators logicalOperators = new LogicalOperators();

		/*
		 * Manda a llamar al método suma de la clase Arithmetics Usa como parámetros los
		 * valores 15 y 13
		 */
		int suma = arithmetics.suma(15, 13);
		System.out.println("Suma: " + suma);

		/*
		 * Manda a llamar al método resta de la clase Arithmetics Usa como parámetros
		 * los valores 15 y 13
		 */
		int resta = arithmetics.resta(15, 13);
		System.out.println("Resta: " + resta);

		/*
		 * Manda a llamar al método multiplicacion de la clase Arithmetics Usa como
		 * parámetros los valores 12 y 24
		 */
		int multiplicacion = arithmetics.multiplicacion(12, 24);
		System.out.println("Multiplicación: " + multiplicacion);

		/*
		 * Manda a llamar al método division de la clase Arithmetics Usa como parámetros
		 * los valores 24 y 12
		 */
		int division = arithmetics.division(24, 12);
		System.out.println("División: " + division);

		/*
		 * Manda a llamar al método modulo de la clase Arithmetics Usa como parámetros
		 * los valores 23 y 12
		 */
		int modulo = arithmetics.modulo(23, 12);
		System.out.println("Modulo: " + modulo);

		/*
		 * Manda a llamar al método mayorMenor de la clase Comparisons Usa como
		 * parámetros los valores 13 y 24
		 */
		String mayorMenor = comparisons.mayorMenor(13, 24);
		System.out.println("Prueba mayor y menor que: " + mayorMenor);

		/*
		 * Manda a llamar al método valoresIguales de la clase Comparisons Usa como
		 * parámetros los valores 13 y 1
		 */
		String igualQue = comparisons.valoresIguales(13, 1);
		System.out.println("Prueba igual que: " + igualQue);

		/*
		 * Manda a llamar al método mayorMenorIgual de la clase Comparisons Usa como
		 * parámetros los valores 53 y 24
		 */
		String menorMayorIgual = comparisons.mayorMenorIgual(53, 24);
		System.out.println("Prueba menor/igual y mayor/igual que: " + menorMayorIgual);

		/*
		 * Manda a llamar al método or de la clase LogicalOperators Usa como parámetros
		 * los valores HOLA y MUNDO
		 */
		String metodoOr = logicalOperators.condicionOr("HOLA", "MUNDO");
		System.out.println("Prueba or: " + metodoOr);

		/*
		 * Manda a llamar al método and de la clase LogicalOperators Usa como parámetros
		 * los valores ACADEMIA y JAVA
		 */
		String metodoAnd = logicalOperators.condicionAnd("ACADEMIA", "JAVA");
		System.out.println("Prueba and: " + metodoAnd);

		/*
		 * Manda a llamar al método not de la clase LogicalOperators Usa como parámetro
		 * el valor REPROBADO
		 */
		String metodoNot = LogicalOperators.condicionNot("REPROBADO");
		System.out.println("Prueba not: " + metodoNot);

		/*
		 * Manda a llamar al método xor de la clase LogicalOperators Usa como parámetros
		 * los valores 15 y 27
		 */
		String metodoXor = LogicalOperators.condicionXor(15, 27);
		System.out.println("Prueba xor: " + metodoXor);
	}
}
