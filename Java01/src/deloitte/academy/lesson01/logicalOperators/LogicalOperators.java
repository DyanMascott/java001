package deloitte.academy.lesson01.logicalOperators;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author dmascott
 *
 */

public class LogicalOperators {
	/*
	 * Se inicializa el objeto LOGGER El LOGGER obtendr� como valor el nombre del
	 * m�todo
	 */
	private static final Logger LOGGER = Logger.getLogger(LogicalOperators.class.getName());

	/**
	 * M�todo condicionOr
	 * 
	 * @param valor1 Valor de tipo String
	 * @param valor2 Valor de tipo String
	 * @return valor Valor de tipo String
	 * 
	 *         Se compara si valor1 es igual al texto "HOLA" o si valor2 es igual al
	 *         texto "MUNDO"
	 */
	public String condicionOr(String valor1, String valor2) {
		String valor = "";
		try {
			if (valor1 == "HOLA" || valor2 == "MUNDO") {
				valor = "Se cumpli� con el OR";
				LOGGER.info("Al menos una condici�n se cumple");
			} else {
				valor = "No se cumpli� con el OR";
				LOGGER.info("No se cumple ninguna condici�n");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

	/**
	 * M�todo condicionAnd
	 * 
	 * @param valor1 Valor de tipo String
	 * @param valor2 Valor de tipo String
	 * @return valor Valor de tipo String
	 * 
	 *         Se compara si valor1 es igual al texto "ACADEMIA" y si valor2 es
	 *         igual al texto "JAVA"
	 */
	public String condicionAnd(String valor1, String valor2) {
		String valor = "";
		try {
			if (valor1 == "ACADEMIA" && valor2 == "JAVA") {
				valor = "Se cumpli� con el AND";
				LOGGER.info("Ambas condiciones se cumplen");
			} else {
				valor = "No se cumpli� con el AND";
				LOGGER.info("No se cumplieron ambas condiciones");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

	/**
	 * M�todo condicionNot
	 * 
	 * @param valor1 Valor de tipo String
	 * @return valor Valor de tipo String
	 * 
	 *         Se compara si valor1 no es igual al texto "APROBADO"
	 */
	public static String condicionNot(String valor1) {
		String valor = "";
		try {
			if (valor1 != "APROBADO") {
				valor = "Se cumpli� con el NOT";
				LOGGER.info("El valor no era igual");
			} else {
				valor = "No se cumpli� con el NOT";
				LOGGER.info("El valor era igual");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

	/**
	 * M�todo condicionXor
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return valor Valor de tipo String
	 * 
	 *         Se compara primero si valor1 no es igual a 15 y si valor2 es igual a
	 *         27 y despu�s se compara si valor1 es igual a 15 y valor2 no es igual
	 *         a 27
	 */
	public static String condicionXor(int valor1, int valor2) {
		String valor = "";
		try {
			if ((valor1 != 15 && valor2 == 27) || (valor1 == 15 && valor2 != 27)) {
				valor = "Se cumpli� con el XOR";
				LOGGER.info("Una condici�n se cumple y la otra no");
			} else {
				valor = "No se cumpli� con el XOR";
				LOGGER.info("Ambas o ninguna de las condiciones se cumple");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

}
