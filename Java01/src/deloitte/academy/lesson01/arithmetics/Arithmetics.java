package deloitte.academy.lesson01.arithmetics;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author dmascott
 *
 */

public class Arithmetics {

	/*
	 * Se inicializa el objeto LOGGER El LOGGER obtendr� como valor el nombre del
	 * m�todo
	 */
	private static final Logger LOGGER = Logger.getLogger(Arithmetics.class.getName());

	/**
	 * M�todo suma
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return suma Valor de tipo entero
	 * 
	 *         Se suman los valores valor1 y valor2 y se guardan en la variable suma
	 */
	public int suma(int valor1, int valor2) {
		int suma = 0;
		try {
			suma = valor1 + valor2;
			LOGGER.info("Se han sumado correctamente los n�meros " + valor1 + " y " + valor2);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return suma;
	}

	/**
	 * M�todo resta
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return resta Valor de tipo entero
	 * 
	 *         Se restan los valores valor1 y valor2 y se guardan en la variable
	 *         resta
	 */
	public int resta(int valor1, int valor2) {
		int resta = 0;
		try {
			resta = valor1 - valor2;
			LOGGER.info("Se han restado correctamente los n�meros " + valor1 + " y " + valor2);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return resta;
	}

	/**
	 * M�todo multiplicacion
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return multiplicacion Valor de tipo entero
	 * 
	 *         Se multiplican los valores valor1 y valor2 y se guardan en la
	 *         variable multiplicacion
	 */
	public int multiplicacion(int valor1, int valor2) {
		int multiplicacion = 0;
		try {
			multiplicacion = valor1 * valor2;
			LOGGER.info("Se han multiplicado correctamente los n�meros " + valor1 + " y " + valor2);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return multiplicacion;

	}

	/**
	 * M�todo division
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return division Valor de tipo entero
	 * 
	 *         Se dividen los valores valor1 y valor2 y se guardan en la variable
	 *         division
	 */
	public int division(int valor1, int valor2) {
		int division = 0;
		try {
			division = valor1 / valor2;
			LOGGER.info("Se han dividido correctamente los n�meros " + valor1 + " y " + valor2);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return division;
	}

	/**
	 * M�todo modulo
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return modulo Valor de tipo entero
	 * 
	 *         Se saca el residuo de la divisi�n de los valores valor1 y valor2 y se
	 *         guardan en la variable modulo
	 */
	public int modulo(int valor1, int valor2) {
		int modulo = 0;
		try {
			modulo = valor1 % valor2;
			LOGGER.info("Se ha sacado correctamente el m�dulo los n�meros " + valor1 + " y " + valor2);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return modulo;
	}

}
