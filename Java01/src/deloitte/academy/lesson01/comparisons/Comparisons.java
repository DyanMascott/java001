package deloitte.academy.lesson01.comparisons;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author dmascott
 *
 */
public class Comparisons {
	/*
	 * Se inicializa el objeto LOGGER El LOGGER obtendr� como valor el nombre del
	 * m�todo
	 */
	private static final Logger LOGGER = Logger.getLogger(Comparisons.class.getName());

	/**
	 * M�todo mayorMenor
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return valor Valor de tipo String
	 * 
	 *         Se compara si valor1 es mayor o menor a valor2
	 */
	public String mayorMenor(int valor1, int valor2) {
		String valor = "";
		try {
			if (valor1 > valor2) {
				valor = "mayor que";
				LOGGER.info(valor1 + " es mayor que " + valor2);
			} else if (valor1 < valor2) {
				valor = "menor que";
				LOGGER.info(valor1 + " es menor que " + valor2);
			} else {
				valor = " no hay n�mero mayor ni menor ";
				LOGGER.info(valor);
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

	/**
	 * M�todo valoresIguales
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return valor Valor de tipo String
	 * 
	 *         Se compara si valor1 es igual o diferente a valor2
	 */
	public String valoresIguales(int valor1, int valor2) {
		String valor = "";
		try {
			if (valor1 == valor2) {
				valor = " son iguales";
				LOGGER.info(valor1 + " y " + valor2 + valor);
			} else if (valor1 != valor2) {
				valor = " son diferentes";
				LOGGER.info(valor1 + " y " + valor2 + valor);
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

	/**
	 * M�todo mayorMenorIgual
	 * 
	 * @param valor1 Valor de tipo entero
	 * @param valor2 Valor de tipo entero
	 * @return valor Valor de tipo String
	 * 
	 *         Se comparan si el valor1 es menor o igual que valor2 o valor1 es
	 *         mayor o igual que valor2
	 */
	public String mayorMenorIgual(int valor1, int valor2) {
		String valor = "";
		try {
			if (valor1 <= valor2) {
				valor = "menor o igual que";
				LOGGER.info(valor1 + " es menor o igual que " + valor2);
			} else if (valor1 >= valor2) {
				valor = "mayor o igual que";
				LOGGER.info(valor1 + " es mayor o igual que " + valor2);
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "La excepci�n es: ", e);
		}
		return valor;
	}

}
